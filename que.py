class Node:
    next = None
    data = None

class Queue:
    first = None
    last = None
    q_size = 0
    N = 1000

    def __init__(self, n):
        self.N = n

    def put(self, d):
        if self.q_size >= self.N:
            #print("Drop first item when queue is full")
            self.get() 
        n = Node()
        n.data = d
        if (self.first == None):
            self.first = n
        else:
            self.last.next = n
        self.last = n
        self.q_size += 1
        return

    def get(self):
        d = self.first.data
        if (self.first.next == None):
            self.q_size -= 1
        self.first = self.first.next
        return d

    def size(self):
        return q_size
    
    def empty(self):
        return self.first == None

class Peekqueue(Queue):
    def peek(self):
        return self.first.data
