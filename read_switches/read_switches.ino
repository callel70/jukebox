// Digital in:
// 0-1: Serial com
// 2-5: Character
// 6-9: Number
// 10:  Select
const int char_0 = 2;
const int char_1 = 3;
//const int char_2 = 4;
//const int char_3 = 5;

const int num_0 = 6;
const int num_1 = 7;
//const int num_2 = 8;
//const int num_3 = 9;

const int select_btn = 10;

// Digital out
const int ledPin =  13;      // Indicates contact with PC

// Constants for serial communication
const int cmd_select = 13;
 
// Buttons states
int charState;         
int lastCharVal = 0;   
int numState;   
// Last button reading 
int lastNumVal = 0;   
int selectState;         
int lastSelect = 0;   
// Timing for last change of button readings
long lastCharValDebounceTime = 0;  
long lastNumValDebounceTime = 0;  
long lastSelectDebounceTime = 0;  
// the debounce time; increase if the output flickers
long debounceDelay = 50;    
  
void setup() 
{ 
  Serial.begin(9600); 

  for (int pin=char_0; pin<=select_btn; pin++)
  {
    pinMode(pin, INPUT);
  }
  
  pinMode(ledPin, OUTPUT);

  digitalWrite(ledPin, LOW);
  establishContact();
} 

void establishContact() {
   while (Serial.available() <= 0) {
     Serial.print('R');   // send a capital A
     delay(300); 
   }
 }

int readNofPins(int startPin, int numPins)
{
  int val = 0;
  for (int i=startPin; i<(startPin+numPins); i++)
  {
    val += digitalRead(i)*(1 + i-startPin);
  }
  return val;
}

void loop() 
{ 
   int pcState = 0;
   if (Serial.available() > 0) {
     pcState = Serial.read();
     if (pcState == 'R') {
       digitalWrite(ledPin, HIGH);
     }
     else {
       digitalWrite(ledPin, LOW);
     }
   }
  
   // read input pins
   int charVal = readNofPins(char_0, 2);
   int numVal = readNofPins(num_0, 2);
   int select = digitalRead(select_btn);

   // Debounce character switches
   if (charVal != lastCharVal) {
     // reset the debouncing timer
     lastCharValDebounceTime = millis();
   } 
   if (((millis() - lastCharValDebounceTime) > debounceDelay) and (charState != charVal)) {
     // whatever the reading is at, it's been there for longer
     // than the debounce delay, so take it as the actual current state:
     charState = charVal;
     if (charVal > 0) {
       Serial.write('a' + charVal - 1);
     }
   }
   lastCharVal = charVal;

   // Debounce numerical switches
   if (numVal != lastNumVal) {
     // reset the debouncing timer
     lastNumValDebounceTime = millis();
   } 
   if (((millis() - lastNumValDebounceTime) > debounceDelay) and (numState != numVal)) {
     // whatever the reading is at, it's been there for longer
     // than the debounce delay, so take it as the actual current state:
     numState = numVal;
     if (numVal > 0) {
       Serial.write('0' + numVal - 1);
     }
   }
   lastNumVal = numVal;

   // Debounce select switch
   if (select != lastSelect) {
     // reset the debouncing timer
     lastSelectDebounceTime = millis();
   } 
   if (((millis() - lastSelectDebounceTime) > debounceDelay) and (selectState != select)) {
     // whatever the reading is at, it's been there for longer
     // than the debounce delay, so take it as the actual current state:
     selectState = select;
     
     if (selectState > 0) {
       Serial.write(cmd_select);
     }
   }
   lastSelect = select;
}
 
 
