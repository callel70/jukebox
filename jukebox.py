import que
import subprocess
from os import listdir
from os.path import *
from sys import argv
import msvcrt
import serial
import time

# Read keyboard
def kbfunc():
    if msvcrt.kbhit():
        ret = ord(msvcrt.getch())
    else:
        ret = 0
    return ret

# Create media array
def ls(path):
    kat=listdir(path)
    arr = []
#    q.put("(")
#    q.put(basename(path))
#    for d in kat:
#        fullpath=path+"/"+d
#        if isdir(fullpath): ls(fullpath)
    for f in kat:
        fullpath=path+"/"+f
        if isfile(fullpath):
            if f.endswith(".mp3"): arr += [[f, realpath(fullpath)]]
#    q.put(")")
    return sorted(arr)

# Create buffer to display song selection info
def show_legend():
    buffer = str("Antal använda alfa knappar: " + str(n_used_chr_buttons) + '\r\n')
    buffer += str("Legend:\r\n")
    buffer += str("knapp - låt kombinationer:\r\n")
    for i in range(len(kat)):
        if i//n_num_btns < n_chr_btns:
            buffer += str(chr_btns[i//n_num_btns]) + str(i%n_num_btns)
        else:
            buffer += "  "
        buffer += ": " + str(kat[i][0]) + "\r\n"
    buffer += str("\nSelect: ") + chr(c_chr_select) + "\r\n"
    buffer += str("Show songs: ") + chr(c_chr_show_songs) + "\r\n"
    buffer += str("Purge queue: ") + chr(c_chr_purge_queue) + "\r\n"
    buffer += str("Quit: ") + chr(c_chr_quit) + "\r\n"
    return buffer

vlc = "C:\\Program Files (x86)\\VideoLAN\\VLC\\vlc.exe"
vlc_option=["--play-and-exit", "--qt-start-minimized", "--one-instance", "--playlist-enqueue", "--quiet"]

c_chr_quit = ord('q')
c_chr_select = 13 # 13 == Enter
c_chr_show_songs = ord('s')
c_chr_purge_queue = ord('w')
c_chr_show_queue = ord('t')

# Transmit characters
c_Tx_running = 'R'
c_Tx_stopped = 'S'

com_port_name = 'COM14'

max_queue_len = 10 # Max nof songs that can be queued
n_chr_btns = 3 # Nof available character buttons
n_num_btns = 3 # Nof available numerical buttons
chr_btns = [chr(i) for i in range(ord('a'), ord('a')+n_chr_btns)]

# Read media lib
if len(argv)>1:
    kat = ls(argv[1])
else:
    kat = ls("./media")
n_used_chr_buttons = len(kat)//n_num_btns
if (len(kat)%n_num_btns > 0):
    n_used_chr_buttons += 1
if n_used_chr_buttons > n_chr_btns:
    n_used_chr_buttons = n_chr_btns

# Print legend
print(show_legend())

q = que.Queue(max_queue_len)
process = None # Reference to vlc process
ch = 0         # Input from keybord or serial port. 0 means no input
ch_btn = None  # Selected character
no_btn = None  # Selected number
ser = None     # Serial port

# Open serial port
try:
    ser = serial.Serial(com_port_name, timeout=0)
except serial.SerialException:
    print("COM port could not be opened. Use keyboard instead.")

# Establish contact with external device
if (ser.isOpen()):
    i = 0
    wait_iterations = 10
    while (ser.inWaiting() == 0 and i<wait_iterations):
        print("Wait for contact with Arduino")
        i += 1
        time.sleep(0.3)
    if (i<wait_iterations):
        if (ser.read().decode() == c_Tx_running):
            print("Contact with Arduino!")
            # Indicate running to arduino
            ser.write(c_Tx_running.encode("utf-8"))
        #buffer = show_legend()
        #ser.write(buffer.encode("utf-8"))
    else:
        print("No contact with Arduino")
        ser.close()

# Main loop
while ch != c_chr_quit: # End on 'q'
    ch = 0
    # Get character
    if (ser.isOpen()):
        ch = ser.read()
        if (len(ch) > 0):
            #print(ch)
            ch = ord(ch)
        else:
            ch = 0
    if ch == 0:
        ch = kbfunc()
    # Check character
    if ch >= ord('a') and ch < ord('a')+ n_used_chr_buttons:
        ch_btn = ch - ord('a')
    elif ch >= ord('0') and ch < ord('0') + n_num_btns:
        no_btn = ch - ord('0')
    elif ch == c_chr_select: # Select on enter
        if (ch_btn == None or no_btn == None):
            print("Välj bokstav och siffra först.")
        else:
            i = n_num_btns*ch_btn + no_btn
            if i >= len(kat):
                print("För hög siffra vald, låt ej tilgänglig.")
            else:
                print("Song put in queue: (",chr(ch_btn+ord('a')), no_btn, ")", kat[i][0])
                q.put(kat[i])
                ch_btn = None
                no_btn = None
    elif ch == c_chr_purge_queue:
        print("Empty queue:")
        while (not q.empty()):
            print(q.get()[0])
    elif ch == c_chr_show_songs:
        print(show_legend())
        
    # Play song
    if not q.empty() and process==None:
        n = q.get()
        print(n[0], " started.")
        current_song = n[0]
        process = subprocess.Popen([vlc] + vlc_option + [n[1]])
    if process!=None:
        if process.poll() != None:
            print("Song finished, ", current_song)
            process=None

    time.sleep(0.2)

# Close serial port
if (ser.isOpen()):
    ser.write(c_Tx_stopped.encode("utf-8"))
    ser.close()
